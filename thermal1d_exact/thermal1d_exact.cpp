// thermal1d_exact.cpp : コンソール アプリケーションのエントリ ポイントを定義します。
//


#include "stdafx.h"

#ifndef M_PI
#define M_PI 3.1415926535
#endif


#ifndef DELTA_T
#define DELTA_T 0.05
#endif



#ifndef N
#define N 101
#endif
#define SERIES_LOOP 10000
#define EPS (1e-8)
#define END_T (1+1e-8)

enum thermal_condition{
	ISOTHERMAL,
	ADIABATIC,
};

double exact(double x, double t, thermal_condition opt) {
	double ans = 0.0;
	if (opt == ISOTHERMAL) {
		for (int i = SERIES_LOOP - 1; i > 0; i--) {
			ans += exp(-i * i * M_PI * M_PI * t) * sin(i * M_PI * x) / i;
		}
		ans = ans * 2 / M_PI + x;
	}
	else {
		for (int i = SERIES_LOOP - 1; i > 0; i--) {
			double n = (2 * i - 1) / 2.0;
			ans += exp(-n * n * M_PI * M_PI * t) * sin(n * M_PI * x) / n;
		}
		ans = ans * 2/ M_PI;
	}
	return ans;

}

int main()
{
	double t,x,ex_iso,ex_adi;
	std::ofstream ofs1 ("exact_iso.dat");
	std::ofstream ofs2 ("exact_adi.dat");

	for (t = 0.0; t < END_T; t += DELTA_T) {	
		ofs1 << "# t=" << t << std::endl;
		ofs2 << "# t=" << t << std::endl;
		for (x = 0.0; x < 1.0 + EPS; x+= 1.0 / (double)(N - 1)) {
			ex_iso	= exact(x, t, ISOTHERMAL);
			ex_adi = exact(x, t, ADIABATIC);
			//std::cout << "exact(" << x << ", " << t << ") = " << ex_adi << std::endl;
			ofs1 << x << " " << ex_iso << std::endl;
			ofs2 << x << " " << ex_adi << std::endl;
		}
		ofs1 << std::endl;
		ofs2 << std::endl;
	}
    return 0;
}

