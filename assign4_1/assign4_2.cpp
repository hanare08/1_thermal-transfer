// assign4_2.cpp : コンソール アプリケーションのエントリ ポイントを定義します。
//
#include "stdafx.h"

#define DELTA_T 0.00002
//#define DELTA_T 0.000051 
// : stable when N=100
// #define DELTA_T 0.000052
// : unstable when N=100
#define T_END 10
#define OUTPUT_INTERVAL 10
#define N 51

// 1-dimension thermal transfer assignment.
// ----
// x  : position in [0, 1]
// T  : temperature
// dt : delta t
// 



//enum thermal_condition{
//	ISOTHERMAL,
//	ADIABATIC,
//};

enum solver_method {
	EULER_FORWARD,
//	EULER_BACKWARD,
	CRANK_NICOLSON,
//	RUNGE_KUTTA_4
};


// initialization
void init(double u[][N]) {
	for (int i = 0; i < N; i++) {
		u[i][0] = 0;
		for (int j = 1; j < N; j++)
			if (i == 0) {
				u[i][j] = 1;
			}
			else {
				u[i][j] = 0;
			}
	}
	u[0][0] = 0.5;

	return;
};

// skip
// output of the result for each step
void output_step(int step, double u[][N], double t, double h)
{
	char buf[32];
	sprintf(buf, "results/temp%04d.dat", step);
	std::ofstream ofs(buf);

//	std::cout << "step : " << step << " simulation_time : " << t << std::endl;

	ofs << "# time = " << t << std::endl;
	ofs << "# x\ty\tt(x,y)" << std::endl;
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			//std::cout << " u[" << i << "][" << j << "]\t= " << u[i][j] << std::endl;
			ofs << j * h << "\t" << i*h << "\t" << u[i][j] << std::endl;
		}
		ofs << std::endl;
	}
	return;
}

//skip 
//void tridiag_init(double a[], double b[], double c[], double h, double dt, thermal_condition opt, solver_method mthd) {
//	if (mthd == CRANK_NICOLSON) {
//		a[0] = 0;
//		b[0] = 1;
//		c[0] = 0;
//		for (int i = 1; i < N - 1; i++) {
//			a[i] = - dt / (2 * h * h);
//			b[i] = 1 + dt / (h * h);
//			c[i] = - dt / (2 * h * h);
//		}
//		if (opt == ISOTHERMAL) {
//			a[N - 1] = 0;
//			b[N - 1] = 1;
//		}		
//		else {
//			a[N - 1] = - dt / (h * h);
//			b[N - 1] = 1 + dt / (h * h);
//		}
//		c[N - 1] = 0;
//	}
//	return;
//
//};

void update_u(double u[][N], double dt, double h, solver_method mthd) {
	
	// Explicit Scheme
// Forward Euler

	if (mthd == EULER_FORWARD) {
		// temporary variables
		double pre_h, pre_v[N], cur;
		pre_h = u[1][0];
		for (int j = 0; j < N; j++) {
			pre_v[j] = u[0][j];
		}

		for (int i = 1; i < N - 1; i++) {
			pre_h = u[i][0];
			for (int j = 1; j < N - 1; j++) {
				cur = u[i][j];
				u[i][j] += (pre_h + pre_v[j] + u[i + 1][j] + u[i][j + 1] - 4 * cur) * dt / (h * h);
				pre_h = cur;
				pre_v[j] = cur;
			}
			// x=1: Adiabatic Boundary Condition
			cur = u[i][N - 1];
			u[i][N - 1] += (2 * pre_h + pre_v[N - 1] + u[i + 1][N - 1] - 4 * cur) * dt / (h * h);
			pre_v[N - 1] = cur;
		}
		
		// y=1: Adiabatic Boundary Condition
		pre_h = u[N - 1][0];
		for (int j = 1; j < N - 1; j++) {
			cur = u[N-1][j];
			u[N-1][j] += (pre_h + 2 * pre_v[j] + u[N-1][j + 1] - 4 * cur) * dt / (h * h);
			pre_h = cur;
		}

		// x=1,y=1: Adiabatic Boundary Condition
        // This value will not be used...
		u[N-1][N - 1] += (2 * pre_h + 2 * pre_v[N-1] - 4 * u[N-1][N-1]) * dt / (h * h);
		
	}
	//else if (mthd == CRANK_NICOLSON) {
	//	double f[N];
	//	f[0] = u[0];
	//	for (int i = 1; i < N - 1; i++)
	//		f[i] = u[i] + (u[i - 1] - 2 * u[i] + u[i + 1]) * dt / (2 * h * h);
	//	if (opt == ISOTHERMAL) {
	//		f[N - 1] = u[N - 1];
	//	}
	//	else {
	//		// Adiabatic // N must be larger than 2
	//		f[N - 1] = u[N - 1] + (2 * u[N - 2] - 2 * u[N - 1]) * dt / (h * h);
	//	}
	//	tridiag_solver(N, a, b, c, f);
	//	for (int i = 0; i < N; i++) {
	//		u[i] = f[i];
	//	}
	//}

	return;
}

// solver
// dt :
// t_end :
// output_interval : 
// thermal_condition : 
// solver_method : 
void solver(double u[][N], double dt, double t_end, double output_interval, solver_method mthd) {
	double t, t_output;
	double h = 1.0 / (double)(N-1);
	int step=0;	

	//tridiag_init(a, b, c, h, dt, opt, mthd);
	//tridiag_solver_init(N, a, b, c);

	t_output = 0;
	output_step(step++, u, 0, h);

	for (t = 0; t < t_end; t += dt) {
		update_u(u, dt, h, mthd);
		t_output += dt;
		if (t_output >= output_interval) {
			t_output -= output_interval;
//			output_step(step++, u, t, h, ofs);
			output_step(step++, u, t, h);
		}

	}

};

void tests() {
	return;
}

int main()
{
	double u[N][N];

	init(u);

	// explicit
	solver(u, DELTA_T, T_END + DELTA_T * DELTA_T, OUTPUT_INTERVAL, EULER_FORWARD);
	
    return 0;
}

//todo
//
