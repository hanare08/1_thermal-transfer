// assign4_2.cpp : コンソール アプリケーションのエントリ ポイントを定義します。
//
#include "stdafx.h"


#ifndef DELTA_T
// #define DELTA_T 0.001
// #define DELTA_T 0.0001
#define DELTA_T 5.00e-5
#define DELTA_STR 5000
#endif
//#define DELTA_T 0.000051 
// : stable when N=100, Euler
// #define DELTA_T 0.000052
// : unstable when N=100, Euler

//#define N 21
//#define N 41
//#define N 61
//#define N 81
#define N 101

#ifndef T_END
#define T_END 1
#endif

#define OUTPUT_INTERVAL 0.05
#define EPS 1e-16
#define SERIES_LOOP 1000

// 1-dimension thermal transfer assignment.
// ----
// x  : position in [0, 1]
// u  : temperature
// dt : delta t
// 


// Boundary condition at one end: x=1
enum thermal_condition{
	ISOTHERMAL,
	ADIABATIC,
};

enum solver_method {
	EULER_FORWARD,
//	EULER_BACKWARD,
	CRANK_NICOLSON,
//	RUNGE_KUTTA_4
};


// initialization
void init(double u[]) {
	for (int i = 1; i < N; i++) {
		u[i] = 1;
	}
	u[0] = 0;

	return;
};

// Estimates exact solution by finite sum of Fourier Series.
// Sums up from smaller value to larger value.
double exact_estimate(double t, double x, thermal_condition opt) {
	double ans = 0.0;
	
	if (opt == ISOTHERMAL) {
		for (int i = SERIES_LOOP - 1; i > 0; i--) {
			ans += exp(-i * i * M_PI * M_PI * t) * sin(i * M_PI * x) / i;
		}
		ans = ans * 2 / M_PI + x;
	}
	else {
		for (int i = SERIES_LOOP - 1; i > 0; i--) {
			double n = (2 * i - 1) / 2.0;
			ans += exp(-n * n * M_PI * M_PI * t) * sin(n * M_PI * x) / n;
		}
		ans = ans * 2/ M_PI;
	}
	return ans;
};

// Writes down the maximum error when time=_t to file stream _efs.
void output_error(double u[], double t, double h, thermal_condition opt, std::ofstream &efs) {
	double err = 0.0;			
	double u_exact;
	for (auto i = 0; i < N; i++) {
		u_exact = exact_estimate(t, i*h, opt);
		err += (u_exact - u[i]) * (u_exact - u[i]);
		// Use relative-error when exact solution is not too small.
	}

	// maximum exact value for this case is always u_exact[N-1]
	err = sqrt(err / N) / u_exact;

//	std::cout << "Max_error in time=" << t << " is " << err << std::endl;
//	std::cout << "Corresponding point is id = " << errid << ", x =  " << errid * h << std::endl;
	efs << t << " " << err << std::endl;
}

// skip
// output of the result for each step
void output_step(int step, double u[], double t, double h, std::ofstream &ofs)
{
	//char buf[32];
	//sprintf(buf, "results/temp%04d.dat", step);
	//std::ofstream ofs(buf);

//	std::cout << "step : " << step << " simulation_time : " << t << std::endl;

	ofs << "# time = " << t << std::endl;
	ofs << "# x\ty\tt(x,y)" << std::endl;

	
	for (int i = 0; i < N; i++) {
//		std::cout << " u[" << i << "]\t= " << u[i] << std::endl;
		ofs << i*h << "\t" << u[i] << std::endl;
	}
	ofs << std::endl;
	return;
}

//skip 
void tridiag_init(double a[], double b[], double c[], double h, double dt, thermal_condition opt, solver_method mthd) {

	if (mthd == CRANK_NICOLSON) {
		double r = dt / (h * h);
		a[0] = 0;
		b[0] = 1;
		c[0] = 0;

		for (int i = 1; i < N - 1; i++) {
			a[i] = -r / 2;
			b[i] = 1 + r;
			c[i] = -r / 2;
		}
		if (opt == ISOTHERMAL) {
			a[N - 1] = 0;
			b[N - 1] = 1;
		}		
		else {
			a[N - 1] = - r;
			b[N - 1] = 1 + r;
		}
		c[N - 1] = 0;
	}
	return;

};

// convert tridiagonal matrix into useful form.
void tridiag_solver_init(double a[], double b[], double c[]) {
	for (int i = 0; i < N; i++) {
		if (i == 0) {
			b[0] = 1.0 / b[0];
		}
		else {
			b[i] = 1.0 / (b[i] - a[i] * c[i - 1]);
			a[i] = b[i] * a[i];
		}
		c[i] = b[i] * c[i];
	}
	return;
}
;

// Linear Equation Solver, using Gauss Elimination.
// Specific for tri-diagonal coefficient matrrix.
// Given Ax=r as 
// a:A[i-1][i]
// b:A[i][i]
// c:A[i+1][i]
void tridiag_solver(double a[], double b[], double c[], double r[]) {
	// forward elimination
	r[0] = b[0] * r[0];
	for (int i = 1; i < N; i++) {
		r[i] = b[i] * r[i] - a[i] * r[i - 1];
	}
	
	// backward substitution
	for (int i = N-2; 0 <= i; i--) {
		r[i] = r[i] - c[i] * r[i + 1];
	}
	return;
}
;

void update_u(double u[], double a[], double b[], double c[], double dt, double h, solver_method mthd, thermal_condition opt) {
	
	// Explicit Scheme
// Forward Euler

	if (mthd == EULER_FORWARD) {
		// temporary variables
		double pre, cur;
		pre = u[0];
		for (int i = 1; i < N - 1; i++) {
			cur = u[i];
			u[i] += (pre - 2 * cur + u[i + 1]) * dt / (h * h);
			pre = cur;
		}
		// x=1: Adiabatic Boundary Condition
		if (opt == ISOTHERMAL) {
			// u[N - 1] = 1;
		}
		else {
			// Adiabatic Boundary Condition
			u[N - 1] += (2 * pre - 2 * u[N-1]) * dt / (h * h);
		}
	}
	   
// Implicit Scheme: Crank Nicolson
	else if (mthd == CRANK_NICOLSON) {
		double f[N];
		f[0] = u[0];
		for (int i = 1; i < N - 1; i++)
			f[i] = u[i] + (u[i - 1] - 2 * u[i] + u[i + 1]) * dt / (2 * h * h);
		if (opt == ISOTHERMAL) {
			f[N - 1] = u[N - 1];
		}
		else {
			// Adiabatic // N must be larger than 2
			f[N - 1] = u[N - 1] + (2 * u[N - 2] - 2 * u[N - 1]) * dt / (2 * h * h);
		}
		tridiag_solver(a, b, c, f);
		for (int i = 0; i < N; i++) {
			u[i] = f[i];
		}
	}

	return;
}

// solver
// dt :
// t_end :
// output_interval : 
// thermal_condition : 
// solver_method : 
void solver(double u[], double dt, double t_end, double output_interval, solver_method mthd, thermal_condition opt) {
	double t, t_output;
	double h = 1.0 / (double)(N-1);
	int step=0;	
	double a[N], b[N], c[N];

	char *opt_string = (opt == ISOTHERMAL ? "iso" : "adi");
	char *mthd_string = (mthd == EULER_FORWARD ? "Eul" : "CNM");

	char buf[64];
	sprintf_s(buf,"result/%s_%s_dt%d_N%d.dat", mthd_string, opt_string, DELTA_STR, N);
	std::ofstream ofs(buf);

	sprintf_s(buf,"error/%s_%s_dt%d_N%d.dat", mthd_string, opt_string, DELTA_STR, N);	
	std::ofstream efs(buf);

	tridiag_init(a, b, c, h, dt, opt, mthd);
	tridiag_solver_init(a, b, c);

	t_output = 0;
	output_step(step++, u, 0, h, ofs);


	for (t = 0; t < t_end; t += dt) {
		update_u(u, a, b, c, dt, h, mthd, opt);

		t_output += dt;
		if (t_output >= output_interval) {
			t_output -= output_interval;
			output_step(step++, u, t, h, ofs);
			output_error(u, t, h, opt,  efs);
		}

		//if (abs(u[N - 1]) < EPS) {
		//	std::cerr << "convergence condition is satisfied when time = " << t << std::endl;
		//	return;
		//}
	}
};

void tests() {
	return;
}

int main()
{
	double u[N];

	// explicit, ISOTHERMAL
	init(u);
	solver(u, DELTA_T, T_END + DELTA_T * DELTA_T, OUTPUT_INTERVAL, EULER_FORWARD, ISOTHERMAL);


	// explicit, ADIABATIC
	init(u);
	solver(u, DELTA_T, T_END + DELTA_T * DELTA_T, OUTPUT_INTERVAL, EULER_FORWARD, ADIABATIC);

	// implicit, ISOTHERMAL
	init(u);
	solver(u, DELTA_T, T_END + DELTA_T * DELTA_T, OUTPUT_INTERVAL, CRANK_NICOLSON, ISOTHERMAL);

	// implicit, ADIBATIC
	init(u);
	solver(u, DELTA_T, T_END + DELTA_T * DELTA_T, OUTPUT_INTERVAL, CRANK_NICOLSON, ADIABATIC);

	
    return 0;
}

//todo
//
