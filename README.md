# README


## Short Summary

* **Learning C++ (1)**: Thermal Equation solver using simple time-step simulation: Euler, Clank-Nicolson.
    - `assign4_1` means 1-dimension version. (explicit, implicit)
    - `assign4_2` means 2-dimension version. (explicit only)
* See <http://www-solid.eps.s.u-tokyo.ac.jp/~ataru/edu/index.html> for the original assignment settings.
* Common powerpoint report on this assignent is on the S***Y.

## Execution (for furture)

* This project was created using Visual Studio Enterprise 2015.